from tkinter import CASCADE
from django.db import models

class Recipe(models.Model):
  name = models.CharField(max_length=125)
  author = models.CharField(max_length=100)
  description = models.TextField()
  image = models.URLField(null=True, blank=True)
  created = models.DateTimeField(auto_now_add=True)
  updated = models.DateTimeField(auto_now=True)

  def __str__(self) -> str:
    return f"Recipe {self.name} by {self.author}"

class Measure(models.Model):
  name = models.CharField(max_length=30, unique=True)
  aabbreviation = models.CharField(max_length=10, unique=True)
  
  def __str__(self) -> str:
    return self.name + ": " + self.aabbreviation

class FoodItem(models.Model):
  name = models.CharField(max_length=100)

  def __str__(self) -> str:
    return self.name

class Ingredient(models.Model):
  amount = models.FloatField()
  recipe = models.ForeignKey('Recipe', related_name='ingredients', on_delete=models.CASCADE, null=True)
  measure = models.ForeignKey('Measure', on_delete=models.PROTECT, null=True)
  food = models.ForeignKey('FoodItem', on_delete=models.PROTECT, null=True)

  def __str__(self) -> str:
    return f"Ingredient is: {self.recipe.name} {self.food.name}"

class Step(models.Model):
  order = models.SmallIntegerField()
  directions = models.CharField(max_length=300)
  recipe = models.ForeignKey('Recipe', related_name='steps', on_delete=models.CASCADE, null=True)    
  Food_Items = models.ManyToManyField("FoodItem", null=True, blank=True)

  def __str__(self) -> str:
    return f"{self.recipe.name} Step {self.order}: {self.directions}  "  

